title: Shubham Mishra (@grapheo12)
template: standalone.html

I am a Ph.D. student at UC Berkeley working under [Prof. Natacha Crooks](https://nacrooks.github.io/) and [Prof. John D. Kubiatowicz](https://people.eecs.berkeley.edu/~kubitron/).
My research interests are in Distributed Systems and Security.

I have been working on Trusted Hardware based consistency systems for a while.

I was an undergrad at IIT Kharagpur, pursuing a B.Tech in Computer Science and Engineering.
I passed out in 2022.

In my leisure time, I watch a lot of web-series and animes and play guitar.
I like to write poems based on current affairs and human emotions.

I am an Open Source Enthusiast and an advisor at [Kharagpur Open Source Society](https://kossiitkgp.org/).
I like to give talks on various tech topics time to time.
Although like a hypocrite, I am using a Macbook to type this.

My resume is [here](https://drive.google.com/file/d/1Y7wpIjEahFHWB9IgKzoTATaoO1g22QSJ/view?usp=sharing).

[Subscribe RSS feed](/feed.xml)
title: Slides and Lectures
template: standalone.html

# PirateShip Project

- [[PDF] Sky Winter Retreat 2025 presentation](https://slides.grapheo12.in/pirateship-sky-retreat.pdf)

# QSC Project

- [[ODP] QSC Presentation](https://slides.grapheo12.in/qsc.odp)

# BTP

- [[PPT] Project Seminar](https://slides.grapheo12.in/btp.html)
- [[PPT] Homomorphic Encryption](https://slides.grapheo12.in/fhe.html)
- [[PPT] Distributed Decryption of Regev Cryptosystem](https://slides.grapheo12.in/regev.html) 

# KOSS seminars

- [[PDF] Basics of Shell Scripting](https://slides.grapheo12.in/bash.pdf)
- [[PDF] Problems on Bash Scripting](https://slides.grapheo12.in/grapheo12/pdf)

# Youtube

- [Video] Python Collections Library: [Part1](https://www.youtube.com/watch?v=LIZX29pDhcc), [Part2](https://www.youtube.com/watch?v=zQdjiUb-WSM), [PDF](/yt-slides/collections.pdf)

# Miscellaneous

- [[PPT] Classical Cryptanalysis](https://slides.grapheo12.in/vigenere.html)

title: Contact Information
template: standalone.html


- Emails: shubham_mishra@berkeley.edu (Primary), grapheo12@gmail.com
- [Github](https://github.com/grapheo12)
- [LinkedIn](https://www.linkedin.com/in/shubham-mishra-195ab1171/)
<!-- - [RSS feed](/feed.xml) -->

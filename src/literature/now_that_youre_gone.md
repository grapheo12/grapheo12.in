title: Now That You Are Gone
template: page.html
date: 2025-01-31
tags: poem

Now that you are gone

My home has gone silent,

My lips have turned vestigial,

My eyes myopic, my nose without air

Clinging onto your last scent.

Now that you are gone

I finally have the space,

The space you asked for, deserved, were denied.

The space so large my screams echo

Back at me, almost laughing, amplified.

Now that you are gone

The streets don't fill up with our banter

Ice-creams sell less.

For no amount of ice-cream can fill your void,

Repair this pathetic mess.

Now that you are gone

I spend days getting drunk in work,

Nights with nothing but counting breath.

Saving enough to pay for the high:

A fraction of your warm embrace.

Now that you are gone

Leaving me for the dead,

This corner of the Internet shall live on.

As a shrine to our closeness.
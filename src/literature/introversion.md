title: Introversion
date: 2022-01-11
template: page.html


Jovial, Friendly

Sitting next to me

Seems from my college,

Should I talk?

Or away should I walk?

I'll initiate once

She's off the phone.

Will I be a creep with this tone?

Lo, she just got off the train.

Oh! the inertia of my brain

And the words

That never left its cage.

<br>

Thirteen thick walls!

Sediments of procrastination

Structured by overthinking

Painted by social awkwardness

And maintained by general disinterest.

Even the closest ones

Lose momentum by the third.

My screams echo back

As pangs of loneliness.

From outside it can't be heard.

Or can it be?

Perhaps people are tired

Trying to help,

Leaving me to choke

In this bliss of solitude.















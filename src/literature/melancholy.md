title: Melancholy
template: page.html
date: 2021-03-03
tags: poem, sadness

I'm not depressed.

Not at all.

But,

When the world snores to rest

And I pull my mind out of stress

Or out of an awesome movie

And sleeping is what I can't be.

At that time,

There's no one to calm my nerves

Out of the countless green dots

The purpose of a friend no one serves.

Then,

I feel a little sad.

A little bit, that's all,

Nothing too bad.

One cell of my heart dies.

Dead cells aren't like autumn leaves,

Are they? I thought I can

Repair them with lies.
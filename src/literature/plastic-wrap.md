title: Plastic Wrap
date: 2024-10-21
template: page.html


Empty nest of thousand acres

Yet the air so still

As if a plastic wrap

With just 11.4 cubic feet of space

Holding me in place

Forbidding me to work

Or relax.

Breathing… again and again and again

Like Sisyphus.

What’s the point in writing this?

Momentary feel-good?

Cry for help?

Plastic wraps can be punctured

Not this one, no,

It self-heals, more air to hold me down.

I try to sleep

Hoping for a lucid dream

Where I’m not like this.

Where the air is not still.

I wake up… why? why?

That world is gone,

Back to here.

Stuck-in-place,

Breathing… again and again and again.
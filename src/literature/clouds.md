title: Clouds
template: page.html
date: 2024-09-28
tags: poem

A cloud over my head.

A sense of calm, body laid to rest.

Flew all the way from the Atlantic?

Just for me?

Subtle confidence and a little pride.

Suddenly the sun pierced my eye.

The cloud moved, letting the fleeting shadow die.

Another cloud that went by,

Another night slowly creeping

To a gloomy death.
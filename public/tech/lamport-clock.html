<!DOCTYPE html>
<html>
    <head>
        <title>Lamport Clocks</title>
        <meta name="tags" content="distributed systems, logical clocks">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/default.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Mono">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro">


<style>
    body {
        font-family: "Source Sans Pro", "Noto Sans", "Noto Sans Bengali", "Monaco";
        text-rendering: optimizeLegibility;
        font-size: 1.2em;
        word-spacing: 0.3em;
    }
</style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/highlight.min.js"></script>
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
        <script>
            window.MathJax = {
                tex: {
                    inlineMath: [['$', '$'], ['\\(', '\\)']]
                }
            };
        </script>
    </head>
    <body>
        <div class="container">
            <div class="tocbox">
                <div class="toc">
<ul>
<li><a href="#problem-with-physical-clocks">Problem with Physical Clocks</a></li>
<li><a href="#logical-clocks">Logical Clocks</a><ul>
<li><a href="#notations">Notations</a></li>
</ul>
</li>
<li><a href="#lamport-clock">Lamport Clock</a><ul>
<li><a href="#total-ordering-of-events">Total Ordering of Events</a></li>
</ul>
</li>
<li><a href="#the-code">The Code</a><ul>
<li><a href="#message">Message</a></li>
<li><a href="#the-node">The Node</a></li>
<li><a href="#putting-it-altogether">Putting it altogether</a></li>
<li><a href="#output">Output</a></li>
</ul>
</li>
<li><a href="#references">References</a></li>
</ul>
</div>

                <div class="tocfollow">
                    <hr>
                    <a href="../"><b>Back</b></a>
                </div>
            </div>
            <div class="content">
                <nav>
                    <h1>Lamport Clocks</h1>
                    <small>2021-05-25</small>
                </nav>
                <h1 id="problem-with-physical-clocks">Problem with Physical Clocks</h1>
<p>There are a lot of good stuff that a wall clock does.
For example, telling you the time. :-)</p>
<p>But when it comes to ordering a set of events,
a simple wall clock will not be sufficient.
Well, it is sufficient, if all of us follow the same clock.
But in a distributed sense it is often not the case.</p>
<p>Consider the following (somewhat phoney) example:</p>
<p>Ram and Shyam meet at Howrah Station
and they sync their clocks with the station clock exactly at 10 am.
Then they go to watch a cricket match at Eden Gardens and sit very far away.
Shyam doesn't know that his watch is faulty and has stopped for an hour in midst of the match.</p>
<p>Ram's job is to note what the bowler does, while Sam records the batsman.</p>
<p>At the end of the day, their notebooks look like these:</p>
<pre><code>[Ram]
3:00:00 PM: Shami gives a powerful yorker! 

[Shyam]
2:00:10 PM: Kohli is out!
</code></pre>
<p>Can you see the problem?
The event of Shami's bowling <strong>happened before</strong> Kohli getting out.
But Shyam's clock was behind, so the event of Kohli getting out received a lower timestamp.
If they merged their notebooks, the resulting log would be incoherent.</p>
<h1 id="logical-clocks">Logical Clocks</h1>
<p>To establish a causal ordering among the events,
the concept of Logical clocks have been formulated.
These do not depend upon Physical clocks
but in general, progress forward in time with new events happening.</p>
<p>Few examples of Logical clocks are: <strong>Lamport Clock</strong>, <strong>Vector Clock</strong> etc.</p>
<p>We will talk about Lamport clocks in this post.</p>
<h2 id="notations">Notations</h2>
<ul>
<li><code>a -&gt; b</code> means event a happened before b</li>
<li><code>a || b</code> means events a and b happened concurrently</li>
<li><code>L(a)</code> is the Lamport Timestamp of an event a.</li>
<li><code>N(a)</code> is the node number on which an event occurs.</li>
</ul>
<h1 id="lamport-clock">Lamport Clock</h1>
<p>This is one of the earliest formulations of Logical clocks and is remarkably simple too.
It was developed by Leslie Lamport.</p>
<p>Each node maintains a counter (<code>t</code>).
The value of <code>t</code> at the time of an event <code>a</code> is the Lamport timestamp of the event <code>L(a)</code>.</p>
<p><code>t</code> is incremented as follows:</p>
<pre><code>// Initial condition
t := 0

// For an internal event in the node
t++
PerformEvent(t)

// Sending message to another node
t++
Send(t, msg)

// Receiving message from another node
t', msg := Recv()
t = max(t, t') + 1
</code></pre>
<p>This simple algorithm ensures that if <code>a -&gt; b</code>, <code>L(a) &lt; L(b)</code>.
The converse, however, is not true.
If <code>L(a) &lt; L(b)</code> then either <code>a -&gt; b</code> or <code>a || b</code>.</p>
<p>It is not possible to distinguish "happens before" and "concurrent" cases using Lamport Clocks.
We need to use the Vector Clock for it.</p>
<h2 id="total-ordering-of-events">Total Ordering of Events</h2>
<p>Lamport clock provides a neat way of establishing a total order among the events.
Since events in different nodes may have the same timestamp,
we define the total order by the pair <code>(L(a), N(a))</code>.</p>
<p>The relation is as follows: <code>a &lt; b &lt;=&gt; (L(a) &lt; L(b)) || (L(a) = L(b) &amp;&amp; N(a) &lt; N(b))</code></p>
<p>This ordering is useful in places where an ordering of events coming from different nodes is required,
eg, in consensus, in distributed database transaction processing etc.</p>
<h1 id="the-code">The Code</h1>
<p>Here is a small example of Lamport Clocks written in Golang.
The nodes are just goroutines and they communicate among themselves using a set of channels.</p>
<p>Each nodes does the following indefinitely:</p>
<ol>
<li>Randomly chooses whether to perform an internal event or send a message</li>
<li>If doing internal event, sleeps for a random amount of time.</li>
<li>If sending message, sends message to a randomly selected node.</li>
</ol>
<h2 id="message">Message</h2>
<p>We send this struct as our message:</p>
<pre><code class="language-go">type Message struct {
    timestamp int32
    message   string
}
</code></pre>
<p>Here <code>timestamp</code> is the Lamport Timestamp.</p>
<h2 id="the-node">The Node</h2>
<p>The node clearly has 2 parts:</p>
<ol>
<li>One that performs the internal event or sends messages</li>
<li>The other that waits to receive a message</li>
</ol>
<p>These 2 have to run concurrently with the internal timestamp (<code>t</code>) being common to both.
Thus the places where <code>t</code> is accessed will become Critical sections.</p>
<pre><code class="language-go">func Node(id int, conn []chan Message, n int) {
    var t int32
    t = 0 // Lamport timestamp

    // Receiving part


    // Event and Sending part
}
</code></pre>
<p>The receiving part is simple:</p>
<pre><code class="language-go">go func() {
    // Receiving section
    for msg := range conn[id] {
        t_temp := int32(math.Max(float64(t), float64(msg.timestamp)))   // max(t, t')
        t_temp++    // max(t, t') + 1
        atomic.StoreInt32(&amp;t, t_temp)
        fmt.Printf(&quot;[Node %d][Time %d] Message received: %s\n&quot;, id, t, msg.message)
    }
}()
</code></pre>
<p>See the use of <code>atomic.StoreInt32</code> for atomic operations on <code>t</code>.</p>
<p>The internal event and sending message part is as follows:</p>
<pre><code class="language-go">for {
    what := rand.Intn(2)
    if what == 0 {
        // Perform an internal event
        d := time.Duration(rand.Int63n((maxSleep + 1)))

        atomic.AddInt32(&amp;t, 1)
        fmt.Printf(&quot;[Node %d][Time %d] Internal Event: Sleeping for %d nanoseconds\n&quot;, id, t, d)
        time.Sleep(d)
        fmt.Printf(&quot;[Node %d][Time %d] Internal Event: Sleeping complete\n&quot;, id, t)
    } else {
        // Send message
        recipient := rand.Intn(n)
        atomic.AddInt32(&amp;t, 1)
        fmt.Printf(&quot;[Node %d][Time %d] Sending message to %d\n&quot;, id, t, recipient)
        message := fmt.Sprintf(&quot;Node %d says Hi!&quot;, id)
        conn[recipient] &lt;- Message{
            timestamp: t,
            message:   message,
        }
        fmt.Printf(&quot;[Node %d][Time %d] Sent message to %d\n&quot;, id, t, recipient)
    }
}
</code></pre>
<h2 id="putting-it-altogether">Putting it altogether</h2>
<p>Here's the full code if someone wants to run it on their machine:</p>
<pre><code class="language-go">/*
File: lamport.go
Author: Shubham Mishra

Implementation of Lamport clocks
Nodes are go routines
*/
package main

import (
    &quot;fmt&quot;
    &quot;math&quot;
    &quot;math/rand&quot;
    &quot;os&quot;
    &quot;strconv&quot;
    &quot;sync&quot;
    &quot;sync/atomic&quot;
    &quot;time&quot;
)

type Message struct {
    timestamp int32
    message   string
}

var maxSleep int64
var wg sync.WaitGroup

/*
Node represents the behaviour of a node.
It randomly chooses whether to perform an internal event
or send message to a randomly selected node.

id = Unique node identifier,
conn = Array of channels used to send message (represents Network),
n = Max nodes
*/
func Node(id int, conn []chan Message, n int) {
    var t int32
    t = 0 // Lamport timestamp

    go func() {
        // Receiving section
        for msg := range conn[id] {
            t_temp := int32(math.Max(float64(t), float64(msg.timestamp)))
            t_temp++
            atomic.StoreInt32(&amp;t, t_temp)
            fmt.Printf(&quot;[Node %d][Time %d] Message received: %s\n&quot;, id, t, msg.message)
        }
    }()

    for {
        what := rand.Intn(2)
        if what == 0 {
            // Perform an internal event
            d := time.Duration(rand.Int63n((maxSleep + 1)))

            atomic.AddInt32(&amp;t, 1)
            fmt.Printf(&quot;[Node %d][Time %d] Internal Event: Sleeping for %d nanoseconds\n&quot;, id, t, d)
            time.Sleep(d)
            fmt.Printf(&quot;[Node %d][Time %d] Internal Event: Sleeping complete\n&quot;, id, t)
        } else {
            // Send message
            recipient := rand.Intn(n)
            atomic.AddInt32(&amp;t, 1)
            fmt.Printf(&quot;[Node %d][Time %d] Sending message to %d\n&quot;, id, t, recipient)
            message := fmt.Sprintf(&quot;Node %d says Hi!&quot;, id)
            conn[recipient] &lt;- Message{
                timestamp: t,
                message:   message,
            }
            fmt.Printf(&quot;[Node %d][Time %d] Sent message to %d\n&quot;, id, t, recipient)
        }
    }

    wg.Done()

}

func main() {
    n, err := strconv.Atoi(os.Args[1])
    if err != nil {
        fmt.Println(&quot;Usage: go run lamport.go number_of_nodes&quot;)
        os.Exit(1)
    }
    conn := make([]chan Message, n)

    for i := 0; i &lt; n; i++ {
        conn[i] = make(chan Message)
    }
    maxSleep = 5e+9
    for i := 0; i &lt; n; i++ {
        wg.Add(1)
        go Node(i, conn, n)
    }
    wg.Wait()

}
</code></pre>
<h2 id="output">Output</h2>
<p>Let us run it with 10 nodes and observe the timestamps in one of the nodes:</p>
<pre><code class="language-bash">go run lamport.go 10 | grep -E &quot;\[Node 1\]&quot;
</code></pre>
<pre><code class="language-log">[Node 1][Time 1] Internal Event: Sleeping for 4712270262 nanoseconds
[Node 1][Time 4] Message received: Node 8 says Hi!
[Node 1][Time 5] Message received: Node 4 says Hi!
[Node 1][Time 6] Message received: Node 2 says Hi!
[Node 1][Time 14] Message received: Node 6 says Hi!
[Node 1][Time 14] Internal Event: Sleeping complete
[Node 1][Time 15] Sending message to 6
[Node 1][Time 15] Sent message to 6
[Node 1][Time 16] Sending message to 1
[Node 1][Time 16] Sent message to 1
[Node 1][Time 17] Internal Event: Sleeping for 4713211716 nanoseconds
[Node 1][Time 18] Message received: Node 1 says Hi!
[Node 1][Time 23] Message received: Node 3 says Hi!
[Node 1][Time 23] Internal Event: Sleeping complete
[Node 1][Time 24] Internal Event: Sleeping for 3108013669 nanoseconds
[Node 1][Time 34] Message received: Node 4 says Hi!
</code></pre>
<p>See how there is a sudden jump in timestamp from 24 to 34 in the last line.</p>
<p>This shows the <code>max</code> operation at work and that node 4 had quite a lot of events happening when node 1 went to sleep for 3 seconds.</p>
<h1 id="references">References</h1>
<p>I have merely scratched the surface.
I mean, what's a post about distributed systems doing without a timing diagram!</p>
<p>These, however, are far better resources:</p>
<ul>
<li><a href="https://en.wikipedia.org/wiki/Lamport_timestamp">Wikipedia</a></li>
<li><a href="https://www.youtube.com/watch?v=x-D8iFU1d-o">Martin Kleppmann's Lecture</a></li>
</ul>
            </div>
        </div>
        <style>
            body {
    background-color: #1f1d1d;
    color: rgb(9, 167, 9);
    display: flex;
    flex-direction: column;
}

nav {
    margin: auto;
    margin-top: 50px;
    margin-bottom: 50px;
}

nav h1 {
    font-size: 3.3rem;
    margin-bottom: 1px;
}

.sidebar {
    display: flex;
    flex-direction: column;
}

.sidebar-content {
    display: flex;
    flex-direction: column;
}


.sidebar-headline {
    display: block;
}

.sidebar-links {
    padding-left: 20px;
    padding-right: 20px;
}


@media screen and (max-width: 480px) {
    .sidebar-content {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }
    
    .sidebar-headline {
        display: none;
    }
    
    .sidebar-end {
        padding-left: 20px;
        padding-right: 20px;
    }
}


.tocfollow, .toc {
    width: 300px;
    position: relative;
}


.tocbox {
    width: 300px;
    display: flex;
    flex-direction: column;
    position: -webkit-sticky;
    position: sticky;
    top: 50%;
    height: max-content;
}

@media screen and (max-width: 480px) {
    .tocbox {
        display: none;
    }

    .content {
        max-width: max-content !important;
    }
}

.toc > ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
}

.toc a:link {
    color: inherit;
    text-decoration: none;
}

.toc a:visited {
    color: inherit;
}

.tocfollow a:link {
    color: inherit;
}

.tocfollow a:visited {
    color: inherit;
}

.container {
    display: flex;
    flex-direction: row;
}

@media screen and (max-width: 480px) {
    .container {
        display: flex;
        flex-direction: column;
    }
    
}

.content {
    max-width: 56%;
    margin-left: auto;
    margin-right: auto;
    padding-right: 50px;
    padding-left: 50px;
    color: rgb(204, 192, 192);
}

.content h1, .content h2, .content h3, .content h4, .content h5, .content h6 {
    color: green;
}

a:link {
    color: cornflowerblue;
    text-decoration: none;
}

a:visited{
    color: cornflowerblue;
    text-decoration: none;
}

pre > code {
    display: block;
    margin: auto;
    background-color: gray;
}

figure {
    width: 70%
}

img {
    max-width: 100%;
    margin-left: auto;
    margin-right: auto;
}

figcaption {
    text-align: center;
    font-weight: bold;
    font-size: 0.7em;
}

blockquote {
    font-size: 1.1em;
    border-left-width: 5px;
    border-left-color: red;
    border-left-style: outset;
    padding-left: 20px;
}

        </style>
        <script>hljs.highlightAll();</script>
        
    </body>
</html>
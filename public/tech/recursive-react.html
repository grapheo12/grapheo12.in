<!DOCTYPE html>
<html>
    <head>
        <title>Easy Form Management in React</title>
        <meta name="tags" content="javascript, react, redux">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/default.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Mono">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro">


<style>
    body {
        font-family: "Source Sans Pro", "Noto Sans", "Noto Sans Bengali", "Monaco";
        text-rendering: optimizeLegibility;
        font-size: 1.2em;
        word-spacing: 0.3em;
    }
</style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/highlight.min.js"></script>
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
        <script>
            window.MathJax = {
                tex: {
                    inlineMath: [['$', '$'], ['\\(', '\\)']]
                }
            };
        </script>
    </head>
    <body>
        <div class="container">
            <div class="tocbox">
                <div class="toc">
<ul>
<li><a href="#understanding-the-structure">Understanding the structure</a></li>
<li><a href="#enter-recursive-react-components">Enter Recursive React Components</a></li>
<li><a href="#wait-but-how-do-i-update-states">Wait! But how do I update states?</a></li>
<li><a href="#talk-is-cheap">Talk is cheap ...</a></li>
<li><a href="#result">Result</a></li>
</ul>
</div>

                <div class="tocfollow">
                    <hr>
                    <a href="../"><b>Back</b></a>
                </div>
            </div>
            <div class="content">
                <nav>
                    <h1>Easy Form Management in React</h1>
                    <small>2020-07-27</small>
                </nav>
                <p>Making a form in React and maintaining it can be painful.
Especially if the form is complicated, with lots of sections and subsections.
Each time a new change needs to be made,
one needs to copy-paste some <code>input</code> tags,
setup default values and configure the state of the form.
If this sounds like an easy thing to do,
wait till your client tells you to put
field <code>A</code> from sub-subsection <code>X</code> of subsection <code>Y</code> of section <code>Z</code>
inside subsection <code>P</code> of section <code>B</code>!
Also, this <a href="https://en.wikipedia.org/wiki/Copypasta">copypasta</a>
pattern goes against the <strong>DRY</strong> (Don't Repeat Yourself) principle.
To get a feel for it, assume you have 100 copy-pasted components,
and you need to change the input type of each one of them.
Nightmare isn't it?</p>
<h2 id="understanding-the-structure">Understanding the structure</h2>
<p>Your whole HTML Webpage, is rendered in the form of a DOM Tree in the browser.</p>
<p>The form is a part of it.
The good thing about a Tree Data Structure is that,
each of the child nodes that arise from a root node,
is a tree in its own right.
Hence a form can be modeled as a tree
and so can be its subsections.</p>
<p><img alt="From Form to Tree" src="/img/FormToGraph.png" /></p>
<p>Now, if you compress the value of each input into a simple JS object
(which you can use as the state of the form)
you get another tree structure, the object-tree.</p>
<p>For the image above, the tree would look like:</p>
<pre><code class="language-ts">{
    a: {
        input1: &quot;value1&quot;,
        input2: &quot;value2&quot;
    },
    input3: &quot;value3&quot;,
    b: {
        input4: &quot;value4&quot;,
        c: {
            input5: &quot;value5&quot;
        }
    }
}
</code></pre>
<p>Notice that if you have a consistent component creation scheme,
(i.e., you create and style all your input fields in a similar way),
this JSON above is all the information you need to create the form.</p>
<h2 id="enter-recursive-react-components">Enter Recursive React Components</h2>
<p>Till now, we haven't seen any React magic happening.
Let's look into it now.
Algorithmically speaking (checkmate Competitive Programmers!),
if we do a <a href="https://www.geeksforgeeks.org/depth-first-search-or-dfs-for-a-graph/">Depth First Traversal</a>
of the JSON object,
generate an appropriate input field for the leaf nodes
and generate section headers for the non-leaf nodes,
we are done.</p>
<p>Abstract pseudo-code for the above strategy is:</p>
<pre><code class="language-ts">
function generate_form(jsonData){
    for (key in jsonData){
        if (jsonData[key] is Object){
            generate_heading(key);
            generate_form(jsonData[key]);
            end_heading(key);
        }else{
            generate_input(name=key, defaultValue=jsonData[key]);
        }
    }
}
</code></pre>
<p>React Components are just JS functions.
This means that the above pseudo-code function can be
directly translated to a functional component.</p>
<h2 id="wait-but-how-do-i-update-states">Wait! But how do I update states?</h2>
<p>Well, there can be many approaches to this.
But please do not directly modify the state object
using references to its child nodes.</p>
<p><img alt="Meme" src="/img/reactlaw.png" /></p>
<p>One method that you can follow is this:</p>
<ol>
<li>Create a <a href="https://stackoverflow.com/questions/184710/what-is-the-difference-between-a-deep-copy-and-a-shallow-copy">deep copy</a> of your state.</li>
<li>Pass this deepcopy to the Recursive Form Component as a scapegoat.</li>
<li>Create a function which when called, simply sets the state
to a deep copy of the above mentioned deepcopy (yes, you need to do a copy of copy).</li>
<li>On change of any field in the form,
directly assign the new value to the concerned field in the deepcopy of step 2 and fire the function mentioned in step 3.</li>
</ol>
<p>The component rendering cycle is as follows:</p>
<p><img alt="Component Rendering Cycle" src="/img/reactflow.svg" /></p>
<p>The reason why we are copying for the 2nd time
is that React does a shallow checking of the states to
decide whether to re-render a component.
Doing a deep copy, will definitely change the reference
of the variable passed.
This will force React to rerender.</p>
<h2 id="talk-is-cheap">Talk is cheap ...</h2>
<p>So, we are done with our <a href="https://en.wikipedia.org/wiki/Waterfall_model">Requirement Analysis and Design</a>,
let's dive into the code.</p>
<p>The code I'm going to share here is from a live project, albeit a little modified.</p>
<p>It uses Accordion, Form and Card Components of the <a href="https://material-ui.com/">Material UI</a> library.
I am not exhaustively including the imports to keep the code brief and to the point.
Instead of using states, here I have used a Redux store for state management.</p>
<p>First, let me show you the recursive component.</p>
<script src="https://gist.github.com/grapheo12/c0f9d54989dd476272fe9d9bdf6f80b7.js?file=GenericForm.js"></script>

<p>The <code>schema</code> is used as the single source of truth for the rendering.
This becomes particularly useful when dealing with arrays (I omitted that part here.)
Here <code>setData</code> is the function described in step 3 above.
The functions <code>display</code> and <code>format</code> come in handy to decide whether to show and how to show an element respectively.
The function <code>keyToFieldName</code> converts camel case like <code>thisIsAField</code> to a space delimited phrase like <code>This Is A Field</code>.</p>
<p>Notice how for the leaf nodes, the value in schema defines the
type of the Input field to be used and also its default value.</p>
<p>Here is how you use the component:</p>
<script src="https://gist.github.com/grapheo12/c0f9d54989dd476272fe9d9bdf6f80b7.js?file=CustomForm.js"></script>

<p>Notice how we are using the <code>cloneDeep</code> function from <code>lodash</code> library to perform the deep copy.</p>
<p><code>dispatch</code> and <code>mapStateToProps</code> are usual Redux jargon.
<code>city</code> is the actual data field on which the form is built.
<code>saveCity</code> is a function that wraps the data to be used
to set the <code>city</code> object inside a Redux action.
<code>defaultCity</code> is the default schema (obvious!).</p>
<h2 id="result">Result</h2>
<p><img alt="Final Result" src="/img/reactresult.png" /></p>
<p>This is what it kinda looks like.
I think you can guess how <code>city</code> data structure looked like.</p>
<p>I am new to Redux.
Let me know in the comments if you found anything wrong here.</p>
            </div>
        </div>
        <style>
            body {
    background-color: #1f1d1d;
    color: rgb(9, 167, 9);
    display: flex;
    flex-direction: column;
}

nav {
    margin: auto;
    margin-top: 50px;
    margin-bottom: 50px;
}

nav h1 {
    font-size: 3.3rem;
    margin-bottom: 1px;
}

.sidebar {
    display: flex;
    flex-direction: column;
}

.sidebar-content {
    display: flex;
    flex-direction: column;
}


.sidebar-headline {
    display: block;
}

.sidebar-links {
    padding-left: 20px;
    padding-right: 20px;
}


@media screen and (max-width: 480px) {
    .sidebar-content {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }
    
    .sidebar-headline {
        display: none;
    }
    
    .sidebar-end {
        padding-left: 20px;
        padding-right: 20px;
    }
}


.tocfollow, .toc {
    width: 300px;
    position: relative;
}


.tocbox {
    width: 300px;
    display: flex;
    flex-direction: column;
    position: -webkit-sticky;
    position: sticky;
    top: 50%;
    height: max-content;
}

@media screen and (max-width: 480px) {
    .tocbox {
        display: none;
    }

    .content {
        max-width: max-content !important;
    }
}

.toc > ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
}

.toc a:link {
    color: inherit;
    text-decoration: none;
}

.toc a:visited {
    color: inherit;
}

.tocfollow a:link {
    color: inherit;
}

.tocfollow a:visited {
    color: inherit;
}

.container {
    display: flex;
    flex-direction: row;
}

@media screen and (max-width: 480px) {
    .container {
        display: flex;
        flex-direction: column;
    }
    
}

.content {
    max-width: 56%;
    margin-left: auto;
    margin-right: auto;
    padding-right: 50px;
    padding-left: 50px;
    color: rgb(204, 192, 192);
}

.content h1, .content h2, .content h3, .content h4, .content h5, .content h6 {
    color: green;
}

a:link {
    color: cornflowerblue;
    text-decoration: none;
}

a:visited{
    color: cornflowerblue;
    text-decoration: none;
}

pre > code {
    display: block;
    margin: auto;
    background-color: gray;
}

figure {
    width: 70%
}

img {
    max-width: 100%;
    margin-left: auto;
    margin-right: auto;
}

figcaption {
    text-align: center;
    font-weight: bold;
    font-size: 0.7em;
}

blockquote {
    font-size: 1.1em;
    border-left-width: 5px;
    border-left-color: red;
    border-left-style: outset;
    padding-left: 20px;
}

        </style>
        <script>hljs.highlightAll();</script>
        
    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <title>Writing serious code in Rust: First impressions</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/default.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Mono">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro">


<style>
    body {
        font-family: "Source Sans Pro", "Noto Sans", "Noto Sans Bengali", "Monaco";
        text-rendering: optimizeLegibility;
        font-size: 1.2em;
        word-spacing: 0.3em;
    }
</style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/highlight.min.js"></script>
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
        <script>
            window.MathJax = {
                tex: {
                    inlineMath: [['$', '$'], ['\\(', '\\)']]
                }
            };
        </script>
    </head>
    <body>
        <div class="container">
            <div class="tocbox">
                <div class="toc">
<ul>
<li><a href="#everything-is-a-typetrait">Everything is a type/trait</a></li>
<li><a href="#pin-your-singletons">Pin your singletons</a></li>
<li><a href="#atomic-updates-of-complicated-data-structures">Atomic updates of complicated data structures</a></li>
<li><a href="#async-programming">Async programming</a></li>
<li><a href="#build-system">Build system</a></li>
</ul>
</div>

                <div class="tocfollow">
                    <hr>
                    <a href="../"><b>Back</b></a>
                </div>
            </div>
            <div class="content">
                <nav>
                    <h1>Writing serious code in Rust: First impressions</h1>
                    <small>2024-09-30</small>
                </nav>
                <blockquote>
<p>This page is still work in progress.</p>
</blockquote>
<p>Recently, I got the chance to intern at Microsoft Research in Cambridge, UK.
It was a great opportunity since I was in the CCF team.
CCF is very close to my research in TEE-based distributed systems
and I got to work on developing a new BFT consensus protocol: PirateShip.
(It is a wordplay on "Raft", the famous consensus protocol, get it?)
I will talk about it some other day. Back to the topic at hand.
I thought, since CCF was built in C++ with OpenEnclave,
and since my research code also used C++ and OpenEnclave, that I will be able to code in it.
To my surprise my manager told me to code the prototype for PirateShip in Rust.
This was a blessing in disguise: I have been putting off learning Rust since I didn't have a project that I could use Rust's concepts on.
But, building this gave me the chance to learn by doing, which I feel is the best way to learn something.</p>
<p>Before you go any further, this blog post is not meant to be some sort of guide.
It is just some useful patterns that I found works in practice.
It may describe anti-patterns as well. I am no Rust expert.
If you feel there is a flaw in my understanding or coding style, feel free to send me an <a href="/contact.html">email</a>.
Lastly, <a href="https://github.com/grapheo12/pirateship">here</a> is a link to the repo if you want to check it out.</p>
<h1 id="everything-is-a-typetrait">Everything is a type/trait</h1>
<p>Rust has created this incredible network of types and traits that make up its type system.
Every behavior is encoded in some trait or type templates.</p>
<ul>
<li>Type can be copied: <code>Copy</code> trait (memcpy everything)</li>
<li>Type can be cloned: <code>Clone</code> trait (<a href="https://stackoverflow.com/questions/31012923/what-is-the-difference-between-copy-and-clone">Different</a> from copy)</li>
<li>Type is allocated in the heap: <code>Box&lt;T&gt;</code></li>
<li>Type is pinned in memory: <code>Pin&lt;T&gt;</code></li>
<li>Type can be ref counted: <code>Rc&lt;T&gt;</code> or <code>Arc&lt;T&gt;</code>.</li>
<li>Type is accessible via a RAII lock: <code>MutexGuard&lt;T&gt;</code> etc.</li>
</ul>
<p>While this is all good for expressiveness and compile-time checking,
it can quickly become quite cumbersome to specify the type of something.
How about an atomic ref counted hashmap of String to String that's pinned in the heap?
<code>Arc&lt;Pin&lt;Box&lt;HashMap&lt;String, String&gt;&gt;&gt;&gt;</code>.</p>
<p>Anytime my types would get too complicated, I got into the habit of creating a new type out of it.
For example,</p>
<pre><code class="language-rust">pub struct PinnedHashMap (pub Arc&lt;Pin&lt;Box&lt;HashMap&lt;String, String&gt;&gt;&gt;&gt;)
</code></pre>
<p>I would also have to define the <code>Deref</code> and <code>DerefMut</code> traits on these types,
so as to not type the <code>.0</code> everytime I had to use them.
Note that, defining <code>type PinnedHashMap = ...</code> also works, but you won't be able to define new methods on it later on if you wanted to.</p>
<h1 id="pin-your-singletons">Pin your singletons</h1>
<p>Networked programs like consensus protocols are mainly developed in the event driven pattern.
There are clear benefits to it, e.g., load balancing, easy backpressure creation, separation of concerns etc.
To implement event driven pattern in Rust, it is easy to use the async programming features.
More on that later.
One aspect of the event-driven architecture is that state is stored in a centralised singleton struct
that every thread/task/coroutine accesses.
However, singletons shared like this must have certain characteristics:</p>
<ol>
<li>Must be available for the entire lifetime of the task.</li>
<li>Must not move around in memory.</li>
<li>Must be accessible without data races.</li>
</ol>
<p>To satisfy condition 1, the struct must be <code>'static</code>, but I think a better solution is to have a ref count.
So, I create my singleton struct as <code>Arc&lt;T&gt;</code> and initiate each task as</p>
<pre><code class="language-rust">spawn(async move || {
    task_fn(ctx.clone())
});
</code></pre>
<p>If <code>ctx</code> is of type <code>Arc&lt;T&gt;</code>, <code>clone()</code> will just increment the ref count and not create a deepcopy.
Furthermore, a clone of the struct is now owned by the task itself, which much simpler to reason about while destructing.</p>
<p>For condition 2, I prefer to <code>Pin&lt;Box&lt;&gt;&gt;</code> the struct.
Only <code>Pin&lt;&gt;</code> should have been fine, but big mutable structs are better suited for heap memory.
Hence the final type of the struct will be <code>Arc&lt;Pin&lt;Box&lt;T&gt;&gt;&gt;</code>.
(I'd also wrap this in a new type, see example above.)</p>
<p>Note that, had this been C++, all of this complexity would be hidden away in the runtime.
You'd pass around a <code>T*</code> which you'd allocate during program startup and delete when it ends.
But everything is Rust has to be expressed via types.</p>
<p>For condition 3, you need either Atomics or Mutexes.
For all simple types, eg, ints and bools, I like to use their atomic versions.
For all sub-structs, we must wrap them in a <code>Mutex</code> or <code>RwMutex</code>.
Access can then only occur through a lock acquire.
If the object is read and written to fairly uniformly, this pattern works.
However, not all structs are like this, some only need occassional updates but is read heavily.
Let's deal with that next.</p>
<h1 id="atomic-updates-of-complicated-data-structures">Atomic updates of complicated data structures</h1>
<p>As I said earlier, some structs are read-heavy (but not read-only).
They need to be updated but very rarely.
As such having a mutex or RwMutex is extra overhead that I don't want to bear.</p>
<p>Atomic Pointers come to rescue.
The logic is as follows:</p>
<ul>
<li>Store the pointer to struct as an <code>AtomicPtr</code>.</li>
<li>Access the pointer with an atomic load.</li>
<li>In order to modify, use Copy on write to create a copy of the struct at another memory location.</li>
<li>Atomically replace the struct pointer when modification is done.</li>
</ul>
<p>Handling raw <code>AtomicPtr</code> is a little dangerous.
Thankfully, the <a href="https://crates.io/crates/crossbeam">crossbeam</a> crate implements some of the functionalities I need.
So I created a helpful utility struct template:</p>
<pre><code class="language-rust">use std::sync::Arc;

use crossbeam::atomic::AtomicCell;


/// AtomicStruct is the atomically settable version of any struct T.
pub struct AtomicStruct&lt;T&gt;(pub Arc&lt;AtomicCell&lt;Arc&lt;Box&lt;T&gt;&gt;&gt;&gt;);

impl&lt;T&gt; AtomicStruct&lt;T&gt; {
    pub fn new(init: T) -&gt; Self {
        Self(Arc::new(AtomicCell::new(Arc::new(Box::new(init)))))
    }

    pub fn get(&amp;self) -&gt; Arc&lt;Box&lt;T&gt;&gt; {
        let ptr = self.0.as_ptr();
        unsafe{ ptr.as_ref().unwrap() }.clone()
    }

    pub fn set(&amp;self, val: Box&lt;T&gt;) {
        self.0.store(Arc::new(val));
    }

    pub fn is_lock_free() -&gt; bool {
        AtomicCell::&lt;Arc&lt;Box&lt;T&gt;&gt;&gt;::is_lock_free()
    }
}

impl&lt;T&gt; Clone for AtomicStruct&lt;T&gt; {
    fn clone(&amp;self) -&gt; Self {
        AtomicStruct(self.0.clone())
    }
}
</code></pre>
<p>For the copy-on-write part, I use <code>Arc::make_mut()</code> on the result I get from the result of <code>get()</code> and then use <code>set()</code> to finalize the update.</p>
<h1 id="async-programming">Async programming</h1>
<h1 id="build-system">Build system</h1>
            </div>
        </div>
        <style>
            body {
    background-color: #1f1d1d;
    color: rgb(9, 167, 9);
    display: flex;
    flex-direction: column;
}

nav {
    margin: auto;
    margin-top: 50px;
    margin-bottom: 50px;
}

nav h1 {
    font-size: 3.3rem;
    margin-bottom: 1px;
}

.sidebar {
    display: flex;
    flex-direction: column;
}

.sidebar-content {
    display: flex;
    flex-direction: column;
}


.sidebar-headline {
    display: block;
}

.sidebar-links {
    padding-left: 20px;
    padding-right: 20px;
}


@media screen and (max-width: 480px) {
    .sidebar-content {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }
    
    .sidebar-headline {
        display: none;
    }
    
    .sidebar-end {
        padding-left: 20px;
        padding-right: 20px;
    }
}


.tocfollow, .toc {
    width: 300px;
    position: relative;
}


.tocbox {
    width: 300px;
    display: flex;
    flex-direction: column;
    position: -webkit-sticky;
    position: sticky;
    top: 50%;
    height: max-content;
}

@media screen and (max-width: 480px) {
    .tocbox {
        display: none;
    }

    .content {
        max-width: max-content !important;
    }
}

.toc > ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
}

.toc a:link {
    color: inherit;
    text-decoration: none;
}

.toc a:visited {
    color: inherit;
}

.tocfollow a:link {
    color: inherit;
}

.tocfollow a:visited {
    color: inherit;
}

.container {
    display: flex;
    flex-direction: row;
}

@media screen and (max-width: 480px) {
    .container {
        display: flex;
        flex-direction: column;
    }
    
}

.content {
    max-width: 56%;
    margin-left: auto;
    margin-right: auto;
    padding-right: 50px;
    padding-left: 50px;
    color: rgb(204, 192, 192);
}

.content h1, .content h2, .content h3, .content h4, .content h5, .content h6 {
    color: green;
}

a:link {
    color: cornflowerblue;
    text-decoration: none;
}

a:visited{
    color: cornflowerblue;
    text-decoration: none;
}

pre > code {
    display: block;
    margin: auto;
    background-color: gray;
}

figure {
    width: 70%
}

img {
    max-width: 100%;
    margin-left: auto;
    margin-right: auto;
}

figcaption {
    text-align: center;
    font-weight: bold;
    font-size: 0.7em;
}

blockquote {
    font-size: 1.1em;
    border-left-width: 5px;
    border-left-color: red;
    border-left-style: outset;
    padding-left: 20px;
}

        </style>
        <script>hljs.highlightAll();</script>
        
    </body>
</html>
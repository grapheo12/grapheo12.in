<!DOCTYPE html>
<html>
    <head>
        <title>Simulating Motion under Gravitation</title>
        <meta name="tags" content="javascript, p5.js">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/default.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Mono">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro">


<style>
    body {
        font-family: "Source Sans Pro", "Noto Sans", "Noto Sans Bengali", "Monaco";
        text-rendering: optimizeLegibility;
        font-size: 1.2em;
        word-spacing: 0.3em;
    }
</style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/highlight.min.js"></script>
        <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
        <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
        <script>
            window.MathJax = {
                tex: {
                    inlineMath: [['$', '$'], ['\\(', '\\)']]
                }
            };
        </script>
    </head>
    <body>
        <div class="container">
            <div class="tocbox">
                <div class="toc">
<ul>
<li><a href="#the-physics-involved">The Physics Involved</a></li>
<li><a href="#approximations-and-rescalings">Approximations and rescalings</a></li>
<li><a href="#the-code-finally">The code, finally</a></li>
</ul>
</div>

                <div class="tocfollow">
                    <hr>
                    <a href="../"><b>Back</b></a>
                </div>
            </div>
            <div class="content">
                <nav>
                    <h1>Simulating Motion under Gravitation</h1>
                    <small>2020-07-20</small>
                </nav>
                <p><img alt="Even 2 body problems can lead to this" src="/img/chaos.png" /></p>
<p>Let's be honest!
I am falling in love with the p5.js framework.
It makes my algorithms and simulations so beautifully presentable!
And that too with so minimum amount of boilerplate.</p>
<p>After building a <a href="/blog/tech/mandelbrot-set/">Mandelbrot set simulator</a>,
I thought, why to stop here, let's build something related to Physics,
which I loved very much during my +2 days.
Then the idea of creating the Gravitation simulator came into my mind.</p>
<h2 id="the-physics-involved">The Physics Involved</h2>
<p>Gravitation comes under (classically) <a href="https://en.wikipedia.org/wiki/Classical_central-force_problem">Central forces</a>.</p>
<p>The magnitude of gravitational force between 2 point masses $m_1$ and $m_2$ separated by a displacement of magnitude $r$ is given by:</p>
<p>$$
F = G \cdot \frac{m_1 \cdot m_2}{r ^ 2}
$$</p>
<p>where $G$ is the <a href="https://en.wikipedia.org/wiki/Gravitational_constant">universal gravitational constant</a>, with value $$G = 6.674 \cdot 10 ^ {-11} m ^ {3} kg ^ {-1} s ^ {-2}$$.</p>
<p>Combining this with Newton's famous 2nd law,
the magnitude of acceleration $a$ of $m_1$ is given by:</p>
<p>$$
a = \frac{F}{m_1} = G \cdot \frac{m_2}{r ^ 2}
$$</p>
<p>Now, actually in space, every celestial body moves towards every other due to the Gravitation.
That'd indeed be an incredible multi-body dynamics problem to solve.
But our scope is pretty simple.
We'll just consider $m_1$ to be movable. Other bodies would be stationary.</p>
<p>Gravitation is always attractive (Shhhh! GTR fans).
What that means is, if, by some magic, $m_2$ was pinned down at a fixed point,
$m_1$ will always move TOWARDS $m_2$ with the acceleration given above.</p>
<p>Note that the acceleration is independent of $m_1$,
so whether we take a rock or a planet,
the path traced out by them would be the same.</p>
<h2 id="approximations-and-rescalings">Approximations and rescalings</h2>
<p>The Gravitation is very weak as a force.
Using $G$ directly in the simulation is not practical.
So for the simulation, I use a different scaling constant,
which I pick by hand
and constrain the acceleration values within some handpicked
bounds, to do away with the problem of division by zero,
when $r=0$.</p>
<p>Another cheating that I do in the simulation,
is that I keep all object's mass as 1.
For the simulator,
$1$ body of mass $m$, is the same as,
$m$ bodies of mass $1$ located at the same point.</p>
<p>I also work in 2D plane and decompose the accelerations
in X and Y axes using the slope of the line joining 2 bodies.</p>
<p>So, if $m_1$ (moving mass) is located at $(x_1,y_1)$ and $m_2$ (fixed mass) is located at $(x_2,y_2)$ ,
the components used are (with a scaling factor of 100):</p>
<p>$$
a_x = \frac{100 \cdot (x_2 - x_1)}{r ^ {\frac{3}{2}}}
$$</p>
<p>$$
a_y = \frac{100 \cdot (y_2 - y_1)}{r ^ {\frac{3}{2}}}
$$</p>
<p>I also clip $a_x$ and $a_y$ between some bounds.</p>
<p>The total acceleration of the mass $m_1$ is the component-wise sum of accelerations over
all the fixed masses. That is to say, at a given time:</p>
<p>$$
\vec{a}_{total} = \Sigma a_x \hat{i} + \Sigma a_y \hat{j}
$$</p>
<p>I'll drop the sum sign from here on.</p>
<p>The last approximation has to do with
how I calculate the velocity and position in the next timestep.</p>
<p>In reality, velocity would be a nice and smooth time integral
of the acceleration and the displacement would be a
time integral of the velocity.</p>
<p>Here I approximate integral by sum.</p>
<p>I take a small timestep $\Delta t$ (in simulation, it has value 1).
Then calculate:</p>
<p>$$
\vec{v}_{next} = \vec{v}_0 + \vec{a} \cdot \Delta t
$$</p>
<p>$$
\vec{r}_{next} = \vec{r}_0 + \vec{v}_{next} \cdot \Delta t
$$</p>
<p>Of course, the calculations are done component-wise.</p>
<h2 id="the-code-finally">The code, finally</h2>
<p>With all the above considerations in mind,
let's jump right into the code.</p>
<script src="https://gist.github.com/grapheo12/1b03fe4f8f43909e8ea7bcdfb798ed4e.js?file=index.html"></script>

<p>The <code>index.html</code> loads the <code>p5.js</code> library as usual.
Notice that I have created fields for initial position and velocity
as well as buttons for starting and resetting the simulation.</p>
<p>The main juice of the code is <code>sketch.js</code>:</p>
<script src="https://gist.github.com/grapheo12/1b03fe4f8f43909e8ea7bcdfb798ed4e.js?file=sketch.js"></script>

<ul>
<li><code>particles</code> array contains the location of the fixed unit masses;</li>
<li><code>position</code> and <code>velocity</code> correspond to the moving mass.</li>
<li><code>started</code> and <code>stuck</code> are simulation states.</li>
<li><code>lines</code> contains the set of points along the path traced by the moving mass.</li>
</ul>
<p>In the <code>draw</code> function, I check if the mouse is pressed WITHIN THE CANVAS,
I add that location to the <code>particles</code> array.</p>
<p>However if <code>started</code> is set to <code>true</code>, I no longer add bodies,
rather I proceed on calculating the next position at every frame.</p>
<p>Finally, I draw the fixed particles as green circles
and the moving particle as a red body which leaves a red trace.</p>
<p>This logic works fine, until the red body from very far off comes
very near a green body.
In this process it gains tremendous velocity and shoots
out of the screen.
To prevent that from happening,
the <code>stuck</code> variable is set to <code>true</code> whenever such stuff happens.
And once set to <code>true</code>, the particle's position is not updated,
it remains stuck.</p>
<p>This sticking behaviour is controlled by the metric in Line 40.
The value to which it is set in the code hardly has any effect.
Change the threshold to 1 or 10 to see the sticking effect.</p>
<!-- ## Bonus

So you just read the whole boring blog!
You deserve a bonus. (Or did you just click the Bonus link in the Contents!).

Here is the full fledged gravity simulator.
Play with it!
Share your interesting trajectories in the comments section.

<br>
<div style="display: flex; flex-direction: row;">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.10.2/p5.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.10.2/addons/p5.sound.min.js"></script>
    <div style="display:flex; flex-direction:column;">
        <input type="number" max="600" min="0" id="posX" placeholder="posX">
        <input type="number" max="600" min="0" id="posY" placeholder="posY">
        <input type="number" max="1000" min="-1000" id="velX" placeholder="velX">
        <input type="number" max="1000" min="-1000" id="velY" placeholder="velY">
        <button onclick="start()">Start</button>
        <button onclick="reset()">Reset</button>
    </div>
    <div id="canvasDiv"></div>
    <script>
        let particles = [];
        let position = [100, 100];
        let velocity = [0, 0];
        let started = false;
        let stuck = false;
        let lines = [];
        let canvas;
        function start() {
            started = true;
        }
        function reset(){
            started = false;
            stuck = false;
            particles = [];
            lines = [];
        }
        function setup(){
            canvas = createCanvas(600, 600);
            canvas.parent("canvasDiv")
        }
        const timestep = 1;
        function draw(){
            let mindist = 100000000;
            background(200);
            if (!started){
                if (mouseIsPressed){
                    if (mouseX >= 0 && mouseX <= 600 && mouseY >= 0 && mouseY <= 600)
                        particles.push([mouseX, mouseY]);
                    console.log(particles)
                }
                position[0] = Number(document.getElementById("posX").value);
                position[1] = Number(document.getElementById("posY").value);
                velocity[0] = Number(document.getElementById("velX").value);
                velocity[1] = Number(document.getElementById("velY").value);
            }else{
                let acc = [0, 0];
                particles.forEach(elt => {
                    let dist_sqr = (elt[0] - position[0]) * (elt[0] - position[0]) + (elt[1] - position[1]) * (elt[1] - position[1]); 
                    if (dist_sqr / (velocity[0] * velocity[0] + velocity[1] * velocity[1]) < 0.1) stuck = true;
                    if (dist_sqr < mindist) mindist = dist_sqr;
                    acc[0] += constrain(10 * (elt[0] - position[0]) / (dist_sqr * Math.sqrt(dist_sqr)), -10, 10);
                    acc[1] += constrain(10 * (elt[1] - position[1]) / (dist_sqr * Math.sqrt(dist_sqr)), -10, 10);
                });
                if (!stuck){
                    velocity[0] += acc[0] * timestep;
                    velocity[1] += acc[1] * timestep;
                    position[0] += velocity[0] * timestep;
                    position[1] += velocity[1] * timestep;
                    lines.push([...position]);
                }
            }
            particles.forEach(elt => {
                fill(100, 200, 100);
                ellipse(elt[0], elt[1], 10, 10);
            });
            for (let i = 0; i < lines.length - 1; i++){
                stroke(255, 0, 0);
                line(lines[i][0], lines[i][1], lines[i+1][0], lines[i+1][1]);
            }
            stroke(0, 0, 0);
            fill(255, 0, 0);
            rect(position[0], position[1], 5, 5);
        }
    </script>
</div>

-->
            </div>
        </div>
        <style>
            body {
    background-color: #1f1d1d;
    color: rgb(9, 167, 9);
    display: flex;
    flex-direction: column;
}

nav {
    margin: auto;
    margin-top: 50px;
    margin-bottom: 50px;
}

nav h1 {
    font-size: 3.3rem;
    margin-bottom: 1px;
}

.sidebar {
    display: flex;
    flex-direction: column;
}

.sidebar-content {
    display: flex;
    flex-direction: column;
}


.sidebar-headline {
    display: block;
}

.sidebar-links {
    padding-left: 20px;
    padding-right: 20px;
}


@media screen and (max-width: 480px) {
    .sidebar-content {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }
    
    .sidebar-headline {
        display: none;
    }
    
    .sidebar-end {
        padding-left: 20px;
        padding-right: 20px;
    }
}


.tocfollow, .toc {
    width: 300px;
    position: relative;
}


.tocbox {
    width: 300px;
    display: flex;
    flex-direction: column;
    position: -webkit-sticky;
    position: sticky;
    top: 50%;
    height: max-content;
}

@media screen and (max-width: 480px) {
    .tocbox {
        display: none;
    }

    .content {
        max-width: max-content !important;
    }
}

.toc > ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
}

.toc a:link {
    color: inherit;
    text-decoration: none;
}

.toc a:visited {
    color: inherit;
}

.tocfollow a:link {
    color: inherit;
}

.tocfollow a:visited {
    color: inherit;
}

.container {
    display: flex;
    flex-direction: row;
}

@media screen and (max-width: 480px) {
    .container {
        display: flex;
        flex-direction: column;
    }
    
}

.content {
    max-width: 56%;
    margin-left: auto;
    margin-right: auto;
    padding-right: 50px;
    padding-left: 50px;
    color: rgb(204, 192, 192);
}

.content h1, .content h2, .content h3, .content h4, .content h5, .content h6 {
    color: green;
}

a:link {
    color: cornflowerblue;
    text-decoration: none;
}

a:visited{
    color: cornflowerblue;
    text-decoration: none;
}

pre > code {
    display: block;
    margin: auto;
    background-color: gray;
}

figure {
    width: 70%
}

img {
    max-width: 100%;
    margin-left: auto;
    margin-right: auto;
}

figcaption {
    text-align: center;
    font-weight: bold;
    font-size: 0.7em;
}

blockquote {
    font-size: 1.1em;
    border-left-width: 5px;
    border-left-color: red;
    border-left-style: outset;
    padding-left: 20px;
}

        </style>
        <script>hljs.highlightAll();</script>
        
    </body>
</html>
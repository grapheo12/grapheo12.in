global figure
def figure(src="", caption=""):
    return f"""<figure>
        <img src="{ src }" alt="{ caption }" style="width:100%" loading="lazy">
        <figcaption>{ caption }</figcaption>
    </figure>"""

global gist
def gist(src="", file=""):
    if file == "":
        return f"""<script src="https://gist.github.com/{ src }.js"></script>"""
    else:
        return f"""<script src="https://gist.github.com/{ src }.js?file={ file }"></script>"""
